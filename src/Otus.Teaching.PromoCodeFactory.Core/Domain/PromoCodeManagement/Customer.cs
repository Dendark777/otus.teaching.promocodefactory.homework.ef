﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(10)]
        public string FirstName { get; set; }
        [MaxLength(10)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(10)]
        public string Email { get; set; }

        //TODO: Списки Preferences и Promocodes 
        public  PromoCode PromoCode { get; set; }
    }
}