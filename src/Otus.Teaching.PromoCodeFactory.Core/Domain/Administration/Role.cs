﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        [MaxLength(10)]
        public string Name { get; set; }

        [MaxLength(10)]
        public string Description { get; set; }
    }
}