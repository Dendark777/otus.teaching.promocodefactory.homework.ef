﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> customersRepository;
        public CustomersController(IRepository<Customer> _customersRepository)
        {
            customersRepository = _customersRepository;
        }


        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
           var customers = await customersRepository.GetAllAsync();
           var customersModelList = customers.Select(item =>
           new CustomerShortResponse()
           {
               Id = item.Id,
               FirstName = item.FirstName,
               LastName = item.LastName,
               Email = item.Email
           }).ToList();
            return Ok(customersModelList);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await customersRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                PromoCodes = new List<PromoCodeShortResponse>()
                {
                    new PromoCodeShortResponse()
                    {
                        Id = Guid.NewGuid(),
                        BeginDate = DateTime.Now.ToString("dd-MM-yyyy"),
                        EndDate = DateTime.Now.AddDays(4).ToString("dd-MM-yyyy"),
                        Code = new Random().Next(1,100).ToString()
                    }
                },
                Email = customer.Email,
            };

            return customerModel;
        }

        [HttpPost]
        public Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            throw new NotImplementedException();
        }

        [HttpPut("{id}")]
        public Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            throw new NotImplementedException();
        }

        [HttpDelete]
        public Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            throw new NotImplementedException();
        }
    }
}