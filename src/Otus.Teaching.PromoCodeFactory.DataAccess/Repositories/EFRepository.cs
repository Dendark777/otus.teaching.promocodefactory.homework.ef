﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EFRepository<T> : IRepository<T> where T : BaseEntity
    {
        private AppDBContext appDBContext;
        internal DbSet<T> dbSet;
        public EFRepository(AppDBContext dbContext)
        {
            appDBContext = dbContext;
            dbSet = appDBContext.Set<T>();
        }

        public Task CreateAsync(T entity)
        {
            appDBContext.Entry<T>(entity).State = EntityState.Added;
            appDBContext.SaveChanges();
            return Task.CompletedTask;
        }


        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(dbSet.AsEnumerable<T>());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public Task Delete(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
